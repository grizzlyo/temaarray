package com.sda.java;

import java.util.Arrays;

public class Logic {
    public int[] subarr(int[] arr, int num) {
        int[] subarr = new int[arr.length];
        int l = 0;
        int suma = 0;
        int sum2 = 0;
        boolean flag = true;
        while (flag) {
            for (int i = l; i < arr.length; i++) {
                suma = suma + arr[i];
                if (suma == num) {
                    subarr = Arrays.copyOfRange(arr, l, i + 1);
                    break;
                }
            }
            l++;
            suma = 0;
            for (int i = 0; i < subarr.length; i++) {
                sum2 = sum2 + subarr[i];
            }
            if (sum2 == num){
                flag = false;
            }
        }
        return subarr;
    }

}
