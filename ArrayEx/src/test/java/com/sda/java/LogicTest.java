package com.sda.java;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;
import sun.rmi.runtime.Log;

/**
 * Unit test for simple ApplicationRunner.
 */
public class LogicTest
{
@Test
    public void shouldAnswerWithTrue()
    {
        System.out.println("Metoda LogicTest a fost apelata!");
        Logic logic = new Logic();
        int arr[] = {1, 3, 7, 8, 9, 12, 14};

        int [] actual = logic.subarr(arr, 18);
        int [] expected = {3,7,8};

        Assert.assertArrayEquals(expected, actual);
    }
}
